<?php	
	session_start();
	function connectDB() {
		$servername = "localhost";
		$username = "root";
		$password = "";
		$dbname = "tokokeren";
		
		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		
		// Check connection
		if (!$conn) {
			die("Connection failed: " + mysqli_connect_error());
		}
		return $conn;
	}

	function submitPromo(){
		$conn = connectDB();

		$deskripsi = $_POST['isiDeskripsi'];
		$periodeAwal = $_POST['periodeAwal'];
		$periodeAkhir = $_POST['periodeAwal'];
		$kodePromo = $_POST['kodePromo'];
		$kategoriUtama = $_POST['kategoriUtama'];
		$subKategori = $_POST['subKategori']; 
		$idPromo;

		$sql4 = "SELECT id FROM PROMO";

		if(!$result4 = mysqli_query($conn, $sql4)) {
			die("Error: $sql");
		}

		while ($row = mysqli_fetch_row($result4)) {
			$idPromo = $row[0];
		}

		echo $idPromo;

		$idPromo += 1;

		echo $idPromo;

		$sql = "INSERT into PROMO (id, deskripsi, periode_awal, periode_akhir, kode) values ('$idPromo','$deskripsi', '$periodeAwal', '$periodeAkhir', '$kodePromo')";

		if(!$result1 = mysqli_query($conn, $sql1)) {
			die("Error: $sql");
		}

		$sql2 = "SELECT * FROM SHIPPED_PRODUK WHERE kategori = $subKategori";

		if(!$result2 = mysqli_query($conn, $sql2)) {
			die("Error: $sql");
		}

		while ($row = mysqli_fetch_row($result2)) {
			$kode_produk = $row[0];
			$sql3 = "INSERT INTO PROMO_PRODUK (id_promo, kode_produk) values ('$idPromo', '$kode_produk')";

			if(!$result3 = mysqli_query($conn, $sql3)) {
				die("Error: $sql3");
			}
		}

		header("Location: index.php");

	}

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
		if($_POST['command'] === 'submit') {
			submitPromo();
		}
	}
?>

<!DOCTYPE html>
<html lang="en">
		<title>TokoKeren - Home to Most Keren Tokoes</title>
		<meta charset="UTF-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="icon" href="../../favicon.ico">
		<link rel = "stylesheet" type = "text/css" href = "bootstrap-4.0.0-alpha.6/dist/css/bootstrap.min.css">
		<link rel = "stylesheet" type = "text/css" href = "src/css/index-style.css">
		<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	</head>
	<body>
		<div class="site-wrapper">

	      <div class="site-wrapper-inner">

	        <div class="cover-container">

	          <div class="masthead clearfix">
	            <div class="inner">
	              <h3 class="masthead-brand"><img id="tokoicon" src="src/icon/credit-card-01.png" alt="Cover"></h3>
	              <nav class="nav nav-masthead">
	                <a class="nav-link active" href="#">Home</a>
	                <a class="nav-link" href="admin.php">Admin</a>
	                <a class="nav-link" href="#">Feature n</a>
	              </nav>
	            </div>
	          </div>

	          <div class="inner cover">
	            <h1 class="cover-heading">Toko<b>Keren</b>.</h1>
	            <p class="lead">Toko<b>Keren</b> provides you the most honest, authentic, quality in every store has to offer</p>
	            <p class="lead">
	              <a href="#" class="btn btn-lg btn-secondary">Get Started</a>
	            </p>
	          </div>

	          <div class="mastfoot">
	            <div class="inner">
	            </div>
	          </div>

	        </div>

	      </div>

	    </div>
		
		<script src="libs/jquery/dist/jquery.min.js"></script>
		<script src="libs/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript">
			function getId(val){
				$.ajax({
					type: "POST",
					url: "getdata.php",
					data: "cid="+val,
					success: function(data){
						$("#subKategori").html(data);
					}
				});
			}
		</script>
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
	    <script src="../../dist/js/bootstrap.min.js"></script>
	    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
	</body>
</html>